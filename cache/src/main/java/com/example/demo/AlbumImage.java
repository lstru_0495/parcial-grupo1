package com.example.demo;

import javax.swing.ImageIcon;

public class AlbumImage {
	private String id;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public ImageIcon getImage() {
		return image;
	}
	public void setImage(ImageIcon image) {
		this.image = image;
	}
	private ImageIcon image;

}
