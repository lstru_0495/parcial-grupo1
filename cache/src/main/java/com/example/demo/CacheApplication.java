package com.example.demo;

import java.awt.EventQueue;

import javax.swing.GroupLayout;
import javax.swing.JFrame;

//import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;

import com.example.gui.AlbumView;
import com.example.gui.Frame;

@EnableCaching
@SpringBootApplication
public class CacheApplication  {

	
	public static void main(String[] args) {
		
		ApplicationContext context = new SpringApplicationBuilder(CacheApplication.class).headless(false).run(args);
		ImageService imageService = context.getBean(ImageService.class);
		

        EventQueue.invokeLater(() -> {
            //Frame ex = new Frame();
            AlbumView ex = new AlbumView();
        	ex.setImageService(imageService);
            ex.setVisible(true);
        });
	}
}
