package com.example.demo;

import java.awt.Image;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

@Component
@CacheConfig(cacheNames = "images")
public class ImageService {

	@Cacheable(key = "#id")
	public AlbumImage getAlbumImage(String id){
		AlbumImage ai = new AlbumImage();
		ai.setId(id);
		try {
			URL url = new URL(id);
			InputStream in = url.openStream();
			Image image = ImageIO.read(in);
			ai.setImage(new ImageIcon(image));
		}catch (IOException e) {
			e.printStackTrace();
		}
		
		return ai;
	}
	
	@CacheEvict(key ="#id")
	public void removeAlbumImage(String id) {
		
	}
		
}
